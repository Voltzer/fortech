package com.example.fortech;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class AddPetActivity extends AppCompatActivity {
    EditText etEmail, etName, etTag, etDescription, etLocation;
    Button imgAddPetBtn;
    ImageButton imgCloseAddPetBtn;
    String Email, Name, Tag, Description, Location;
    DatabaseReference ref;
    DBHelper helper = new DBHelper();

    FirebaseAuth mAuth;
    Dialog myDialogLogin;

    public BluetoothAdapter mBluetoothAdapter;
    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    ArrayAdapter devicesArrayAdapter;
    ArrayList<String> devicesArrayList;
    ListView listView;
    String pet_name;

    Timer timer;
    TimerTask timerTask;
    public boolean okName = false;

    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Log.d(TAG, "onReceive: ACTION FOUND.");

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                getName(device.getAddress(), device);

            }
        }
    };


    public void addPet(final String Tag)
    {
        mAuth = FirebaseAuth.getInstance();
       imgAddPetBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Email = mAuth.getCurrentUser().getEmail();
               Name = etName.getText().toString();
               Description = etDescription.getText().toString();
               Location = etLocation.getText().toString();

               if(Name.isEmpty()){
                   etName.setError("Name is required!");
                   etName.requestFocus();
                   return;
               }

               if(Name.contains("|") || Name.contains("~"))
               {
                   etName.setError("Name cannot contain characters | or ~");
                   etName.requestFocus();
                   return;
               }

               if(Description.contains("|") || Description.contains("~"))
               {
                   etDescription.setError("Description cannot contain characters | or ~");
                   etDescription.requestFocus();
                   return;
               }

               ref = FirebaseDatabase.getInstance().getReference().child("Pets");

               Pets pet = new Pets(mAuth.getCurrentUser().getUid(),Email, Name, Tag, Description, Location);
               ref.push().setValue(pet);
               myDialogLogin.dismiss();
               AddPetActivity.super.finish();

           }
       });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_add_pet);

        devicesArrayList = new ArrayList<String>();
        devicesArrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,devicesArrayList);

        listView = findViewById(R.id.listviewPets);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        startTimer();





        devicesArrayList = new ArrayList<String>();
        devicesArrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,devicesArrayList);

        listView.setAdapter(devicesArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                popupAddPet(devicesArrayList.get(i));
            }
        });

    }

    public void popupAddPet(String tag){

        myDialogLogin= new Dialog(this);
        myDialogLogin.setContentView(R.layout.activity_add_pet);
        myDialogLogin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialogLogin.show();

        etName = myDialogLogin.findViewById(R.id.etName);

        etDescription = myDialogLogin.findViewById(R.id.etDescription);
        etLocation = myDialogLogin.findViewById(R.id.etLocation);
        imgAddPetBtn = myDialogLogin.findViewById(R.id.imgAddDogBtn);
        imgCloseAddPetBtn = myDialogLogin.findViewById(R.id.ImgBtnCloseAddPetBtn);
        imgCloseAddPetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialogLogin.dismiss();
            }
        });
        ref = FirebaseDatabase.getInstance().getReference().child("Pets");
        addPet(tag);

    }
    public void getName(final String Tag, final BluetoothDevice device){
        ref = FirebaseDatabase.getInstance().getReference();
        final String[] PetName = new String[1];
        pet_name = "";
        okName = true;
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!okName)
                    return;
                okName = false;
                Iterable<DataSnapshot> children = dataSnapshot.getChildren();

                boolean nameFound = false;
                for (DataSnapshot child : children) {
                    Pets pet = child.getValue(Pets.class);
                    if (pet.tag.equals(Tag)) {
                        PetName[0] = pet.petname;
                        pet_name = PetName[0];
                        nameFound = true;
                        //return;
                    }
                }

                if (device.getAddress().matches("FF:FF:C5:..:..:..") && !mBTDevices.contains(device) && !nameFound)
                {
                    mBTDevices.add(device);
                    AddToList(device.getAddress());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    protected void AddToList(String text) {
        devicesArrayList.add(text);
        devicesArrayAdapter.notifyDataSetChanged();
    }

    public void btnDiscover(View view) {

        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();

            view.setBackgroundResource(R.drawable.paw_imag_800);
        }
        else {
            if(!checkBTPermissions())
                return;

            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);


            view.setBackground(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private boolean checkBTPermissions() {

        if (!mBluetoothAdapter.isEnabled()) {
            Intent startBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(startBluetooth, 0);
            //mBluetoothAdapter.enable();
            return false;
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }
        return true;
    }

    void makeTOAST(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    void startTimer() {

        timer = new Timer();

        timerTask = new TimerTask() {
            private Handler updateUI = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);

                    if(!mBluetoothAdapter.isDiscovering())
                        (findViewById(R.id.buttonScan)).setBackgroundResource(R.drawable.paw_imag_800);
                }
            };

            public void run() {
                try {
                    updateUI.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer.schedule(timerTask, 3000, 3000);
    }


}
