package com.example.fortech;

import androidx.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Method;

public class DBHelper {
    private static FirebaseDatabase fireDB;
    private static DatabaseReference ref,ref1;
    private static FirebaseAuth mAuth;

    public static String name;
    public static User user = new User();

    public static void getName(final String Tag){
        ref = FirebaseDatabase.getInstance().getReference();

        final String[] PetName = new String[1];
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();

                for (DataSnapshot child:children) {
                    Pets pet = child.getValue(Pets.class);
                    if(pet.tag.equals(Tag))
                    {
                        PetName[0] = pet.petname;
                        name = PetName[0];

                        return;
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
    public static void getUser(final String Tag)
    {
        ref = FirebaseDatabase.getInstance().getReference();

        final String[] ownerKey = new String[1];
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();

                for (DataSnapshot child:children) {
                    Pets pet = child.getValue(Pets.class);
                    if(pet.tag.equals(Tag))
                    {
                        ownerKey[0] = pet.ownerKey;
                        ref1 = FirebaseDatabase.getInstance().getReference("Users/"+ownerKey[0]);
                        ref1.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                user = dataSnapshot.getValue(User.class);

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        return;
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
