package com.example.fortech;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

public class EditCredentials extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private static DatabaseReference ref;

    EditText etEmail, etFirstName, etSecondName, etPhone, etAddress, etPassword, etConfirmPW;
    String email, firstName, secondName, phone, address,   password, Cpassword, currUid;
    boolean cphone, caddress, cname;
    Switch cbIsPhonePublic, cbIsAddressPublic, cbIsNamePublic;
    Button btnEditCredentials;
    Button reset_email, reset_pass;
    User user;

    private void filler()
    {
        etFirstName.setText(user.firstName);
        etSecondName.setText(user.secondName);
        etPhone.setText(user.phone);
        etAddress.setText(user.address);
        cbIsAddressPublic.setChecked(user.isAddressPublic);
        cbIsNamePublic.setChecked(user.isNamePublic);
        cbIsPhonePublic.setChecked(user.isPhonePublic);

    }

    private void getInfo()
    {

        ref = FirebaseDatabase.getInstance().getReference("Users/"+mAuth.getCurrentUser().getUid());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                filler();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void EditUser()
    {
        //password = user.;
        //Cpassword = etConfirmPW.getText().toString();
        firstName = etFirstName.getText().toString();
        secondName = etSecondName.getText().toString();
        phone = etPhone.getText().toString();
        address = etAddress.getText().toString();
        cphone = cbIsPhonePublic.isChecked();
        caddress = cbIsAddressPublic.isChecked();
        cname = cbIsNamePublic.isChecked();

        /**if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError("Please enter a vaild email!");
            etEmail.requestFocus();
            return;
        }
        if(password.isEmpty()){
         etPassword.setError("Please enter a password!");
         etPassword.requestFocus();
         return;
         }
         if(Cpassword.isEmpty()){
         etConfirmPW.setError("Please confirm your password!");
         etConfirmPW.requestFocus();
         return;
         }
         if(!password.equals(Cpassword)){
         etConfirmPW.setError("The password does not match!");
         etConfirmPW.requestFocus();
         return;
         }**/

        if(firstName.isEmpty()){
            etFirstName.setError("Please enter your first name!");
            etFirstName.requestFocus();
            return;
        }
        if(secondName.isEmpty()){
            etSecondName.setError("Please enter your second name!");
            etSecondName.requestFocus();
            return;
        }
        if(phone.isEmpty() && cphone){
            etPhone.setError("Pleasse enter your phone number!");
            etPhone.requestFocus();
            return;
        }
        if(address.isEmpty() && caddress){
            etAddress.setError("Please enter your address!");
            etAddress.requestFocus();
            return;
        }

        User up_user = new User(FirebaseAuth.getInstance().getCurrentUser().getEmail(), firstName, secondName, phone, address, cphone, cname, caddress,user.token);


        FirebaseDatabase.getInstance().getReference("Users")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(up_user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(EditCredentials.this, "Update complete!", Toast.LENGTH_LONG).show();
                    EditCredentials.super.finish();
                }
                else Toast.makeText(EditCredentials.this, "Error!", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void EditPass()
    {
        mAuth.sendPasswordResetEmail(mAuth.getCurrentUser().getEmail()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){

                    Toast.makeText(EditCredentials.this, "We have sent you instructions to reset your password!", Toast.LENGTH_LONG).show();
                    EditCredentials.super.finish();

                } else {
                    Toast.makeText(EditCredentials.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_credentials);


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();

        // DatabaseReference myRef = mFirebaseDatabase.getReference("message");

        // myRef.setValue("Hello, World!");

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPW = findViewById(R.id.etConfirmPW);
        etFirstName = findViewById(R.id.etFirstName);

        etSecondName = findViewById(R.id.etSecondName);
        etPhone = findViewById(R.id.etPhone);
        etAddress = findViewById(R.id.etAddress);
        btnEditCredentials = findViewById(R.id.btnEditCredentials);
        cbIsPhonePublic = findViewById(R.id.cbPhone);
        cbIsAddressPublic = findViewById(R.id.cbAddress);
        cbIsNamePublic = findViewById(R.id.cbName);
        reset_pass = findViewById(R.id.reset_pass);


        cbIsNamePublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsNamePublic.setText("Your name is public.");
                }
                else cbIsNamePublic.setText("Your name is not public.");
            }
        });


        cbIsAddressPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsAddressPublic.setText("Your address is public.");
                }
                else cbIsAddressPublic.setText("Your address is not public.");
            }
        });


        cbIsPhonePublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsPhonePublic.setText("Your phone is public.");
                }
                else cbIsPhonePublic.setText("Your phone is not public.");
            }
        });

        //   currUid = mAuth.getCurrentUser().getUid();
        //getInfo();

        getInfo();

        btnEditCredentials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditUser();
            }
        });
        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditPass();
            }
        });
    }

}
