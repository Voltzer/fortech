package com.example.fortech;

import androidx.core.util.Pair;

import java.util.ArrayList;
import java.util.Random;

public class Encryption {
    //***************************************************************************************
    //rotoare
    private static int[][] Rotors = { { 3, 35, 64, 36, 49, 43, 52, 69, 13, 17, 47, 53, 4, 16, 85, 39, 40, 77, 23, 67, 58, 14, 44, 18, 74, 20 },
        { 92, 71, 55, 23, 13, 8, 91, 42, 86, 81, 35, 66, 64, 39, 47, 9, 65, 90, 56, 6, 69, 51, 87, 1, 24, 75 },
        { 3, 71, 48, 61, 15, 89, 38, 30, 66, 60, 67, 5, 47, 77, 24, 13, 11, 40, 54, 39, 23, 9, 57, 32, 90, 33 },
        { 38, 89, 32, 80, 30, 62, 4, 21, 51, 79, 25, 42, 85, 55, 16, 73, 24, 70, 9, 75, 31, 45, 48, 64, 59, 71 },
        { 49, 38, 22, 27, 55, 92, 31, 63, 57, 13, 6, 61, 93, 18, 40, 4, 7, 26, 30, 8, 15, 76, 29, 9, 68, 83 },
        { 12, 35, 79, 92, 3, 17, 63, 59, 18, 67, 9, 42, 87, 52, 45, 48, 56, 36, 76, 64, 10, 66, 62, 58, 47, 75 },
        { 73, 32, 30, 57, 86, 75, 39, 21, 93, 92, 29, 83, 55, 74, 78, 40, 3, 31, 90, 51, 61, 59, 56, 76, 71, 49 },
        { 41, 65, 10, 42, 49, 29, 15, 81, 19, 52, 58, 56, 13, 68, 3, 69, 90, 34, 71, 40, 20, 30, 7, 48, 8, 35 },
        { 15, 28, 54, 76, 66, 13, 59, 24, 23, 2, 83, 9, 62, 65, 55, 80, 21, 86, 78, 34, 57, 45, 37, 6, 5, 60 },
        { 60, 45, 6, 15, 46, 65, 82, 10, 37, 86, 68, 12, 43, 75, 49, 57, 66, 51, 7, 28, 71, 9, 53, 18, 16, 61 } };

    private static int[] usedRotors = new int[6];
    private static int[] RotorRotation = new int[6];
    private static String cypher;
    private static Random r = new Random();

    private static char rotorInput(char c1)
    {
        char c2;
        c2 = (char)(((int)c1 - 32 + Rotors[usedRotors[0]][RotorRotation[0]] + Rotors[usedRotors[1]][RotorRotation[1]] + Rotors[usedRotors[2]][RotorRotation[2]] + Rotors[usedRotors[3]][RotorRotation[3]] + Rotors[usedRotors[4]][RotorRotation[4]]) % 95 + 32);
        RotorRotation[0]++;
        if(RotorRotation[0] >= 26)
        {
            RotorRotation[0] = 0;
            RotorRotation[1]++;
            if (RotorRotation[1] >= 26)
            {
                RotorRotation[1] = 0;
                RotorRotation[2]++;
                if (RotorRotation[2] >= 26)
                {
                    RotorRotation[2] = 0;
                    RotorRotation[3]++;
                    if (RotorRotation[3] >= 26)
                    {
                        RotorRotation[3] = 0;
                        RotorRotation[4]++;
                        if (RotorRotation[4] >= 26)
                        {
                            RotorRotation[4] = 0;
                        }
                    }
                }
            }
        }
        return c2;
    }

    //rotoare
    //***********************************************************************************************
    //perechi

    private static char[][] perechi = new char[2][50];
    private static boolean[] fr = new boolean[95];

    private static char swap(char c)
    {
        for(int i = 0; i<45; i++)
        {
            if(c == perechi[0][i])
            {
                return perechi[1][i];
            }
            if(c == perechi[1][i])
            {
                return perechi[0][i];
            }
        }
        return c;
    }

    //perechi
    //***********************************************************************************************

    public static Pair<String, String> Encryt(String stringToEncrpt) {
        int x, y;
        cypher = "";
        for (int i = 0; i < 5; i++)
        {
            usedRotors[i] = r.nextInt(10);
            RotorRotation[i] = r.nextInt(26);
            cypher += (char)(usedRotors[i]+33);
            cypher += (char)(RotorRotation[i]+33);
        }
        for (int i = 0; i < 95; i++)
            fr[i] = false;
        for(int i = 0; i<45; i++)
        {
            x = r.nextInt(95);
            while(fr[x])
            {
                x = r.nextInt(95);
            }
            fr[x] = true;
            y = r.nextInt(95);
            while (fr[y])
            {
                y = r.nextInt(95);
            }
            fr[y] = true;
            perechi[0][i] = (char)(x + 32);
            perechi[1][i] = (char)(y + 32);
            cypher += perechi[0][i] + "" + perechi[1][i];
        }
        String encryptedString = "";
        for (char c: stringToEncrpt.toCharArray()) {
            encryptedString += swap(rotorInput(swap(c)));
        }
        return Pair.create(encryptedString, cypher);
    }

    public static String Encryt(String stringToEncrpt, String cypher)
    {
        int x, y;
        for (int i = 0; i < 5; i++)
        {
            usedRotors[i] = (int)cypher.toCharArray()[i * 2] - 33;
            RotorRotation[i] = (int)cypher.toCharArray()[i * 2 + 1] - 33;
        }
        for (int i = 10; i < 100; i += 2)
        {
            perechi[0][(i - 10) / 2] = cypher.toCharArray()[i];
            perechi[1][(i - 10) / 2] = cypher.toCharArray()[i+1];
        }

        String encryptedString = "";
        for (char c: stringToEncrpt.toCharArray()) {
            encryptedString += swap(rotorInput(swap(c)));
        }
        return encryptedString;
    }
}
