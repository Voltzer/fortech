package com.example.fortech;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class LogInActivity extends AppCompatActivity {

    EditText etEmail, etPassword;
    Button imgLoginBtn,imgLogoutBtn;
    String password,email;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mFirebaseDatabase;

    // ==========================================

    public void login()
    {
        password = etPassword.getText().toString();
        email = etEmail.getText().toString();

        if(mAuth.getCurrentUser() == null) {
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful())
                        Toast.makeText(LogInActivity.this, "Logged In", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(LogInActivity.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
        else
            Toast.makeText(LogInActivity.this, "Already logged in!", Toast.LENGTH_LONG).show();

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        imgLoginBtn = findViewById(R.id.imgLoginBtn);


        mAuth = FirebaseAuth.getInstance();

        imgLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();

            }
        });

    }
}
