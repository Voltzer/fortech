package com.example.fortech;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.Permission;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FusedLocationProviderClient fusedLocationClient;
    private FirebaseDatabase fireDB;
    private DatabaseReference ref,ref1;
    private FirebaseAuth mAuth;

    public String name;
    public User user = new User();

    EditText etEmail, etPassword;
    Button imgLoginBtn;
    ImageButton imgCloseLoginBtn, imgCloseShowProfile;
    String password,email;

    Dialog myDialogLogin, showProfile;
    public BluetoothAdapter mBluetoothAdapter;
    public ArrayList<BluetoothDevice> mBTDevices = new ArrayList<>();
    ArrayAdapter devicesArrayAdapter;
    ArrayList<String> devicesArrayList;
    ArrayList<String> petDescriptions = new ArrayList<>();
    ListView listView;

    TextView showname, showemail, showphone, showadress;
    Timer timer;
    TimerTask timerTask;
    FileOutputStream profileHistory;
    ProgressBar progLogin;


    public int i2 = 0;

    public boolean okGetUserPet = false, okGetUserUser = false, okGetName = false, okLogin = false, okLocation = false, okRefresh = false;

    public class LocationDB{
        public String longit, latit;
        public LocationDB(){

        }
        public LocationDB(String longi, String lati){
            this.longit = longi;
            this.latit = lati;
        }
    }

    LocationDB loc_add;

    private BroadcastReceiver mBroadcastReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Log.d(TAG, "onReceive: ACTION FOUND.");F

            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (device.getAddress().matches("FF:FF:C5:..:..:..") && !mBTDevices.contains(device)) {
                    mBTDevices.add(device);
                    getName(device.getAddress());
                }


            }
        }
    };

    public void getName(final String Tag){
        ref = FirebaseDatabase.getInstance().getReference();
        final String[] PetName = new String[1];
        okGetName = true;
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!okGetName)
                    return;
                okGetName = false;
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();

                for (DataSnapshot child:children) {
                    Pets pet = child.getValue(Pets.class);
                    if(pet.tag.equals(Tag) && pet.isLost)
                    {
                        PetName[0] = pet.petname;
                        name = PetName[0];
                        AddToList(name + "|" + Tag + "~" + pet.description);
                        return;
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    NavigationView mNavView;
    View headerNavbar;

    TextView tvNameNavbar, tvEmailNavbar;

    boolean searchfornavname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*for(String fileName : fileList())
        {
            if(fileName.equals("profileHistory"))
            {
                try {
                    profileH = readJsonStream(openFileInput(fileName));
                    deleteFile(fileName);
                    break;
                }catch (FileNotFoundException e)
                {
                    Toast.makeText(this, "File not found", Toast.LENGTH_SHORT);
                }catch (IOException e)
                {
                    Toast.makeText(this, "Input stream error", Toast.LENGTH_SHORT);
                }
            }
        }*/


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION }, 0);
        }



        listView = findViewById(R.id.listview);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //mBluetoothAdapter.cancelDiscovery();

        createNotificationChannel();
        startTimer();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        try {
            VariableHolder.profileH = readProfileStream(openFileInput("profileHistory"));
        }catch (FileNotFoundException e) {
        }
        try {
            VariableHolder.notifH = readProfileStream(openFileInput("notifHistory"));
        }catch (FileNotFoundException e) {
        }

        devicesArrayList = new ArrayList<String>();
        devicesArrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,devicesArrayList);

        listView.setAdapter(devicesArrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(MainActivity.this, devicesArrayList.get(i).substring(devicesArrayList.get(i).indexOf('|')+1), Toast.LENGTH_LONG).show();

                String dog_tag = petDescriptions.get(i).substring(petDescriptions.get(i).indexOf('|')+1, petDescriptions.get(i).indexOf('~'));

                getUser(dog_tag);

                try {
                    VariableHolder.profileH = readProfileStream(openFileInput("profileHistory"));
                    deleteFile("profileHistory");
                }catch (FileNotFoundException e)
                {

                }
                if(!VariableHolder.profileH.contains(petDescriptions.get(i)))
                    VariableHolder.profileH.add(petDescriptions.get(i));
                try {
                    profileHistory = openFileOutput("profileHistory", Context.MODE_PRIVATE);
                    writeProfileStream(profileHistory, VariableHolder.profileH);
                }catch (FileNotFoundException e)
                {

                }
            }
        });


        mAuth = FirebaseAuth.getInstance();

        mNavView = findViewById(R.id.nav_view);
        headerNavbar = mNavView.getHeaderView(0);
        Menu menu = mNavView.getMenu();
        MenuItem register_item = menu.findItem(R.id.nav_register);
        MenuItem login_item = menu.findItem(R.id.nav_login);
        MenuItem history = menu.findItem(R.id.nav_history);
        tvNameNavbar = headerNavbar.findViewById(R.id.tvNameNavBar);
        tvEmailNavbar = headerNavbar.findViewById(R.id.tvEmailNavBar);
        if(mAuth.getCurrentUser() != null){

            tvEmailNavbar.setText(mAuth.getCurrentUser().getEmail());
            searchfornavname = true;
            FirebaseDatabase.getInstance().getReference().child("Users/"+ mAuth.getCurrentUser().getUid())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(!searchfornavname)
                                return;
                            User userr = dataSnapshot.getValue(User.class);
                            tvNameNavbar.setText(userr.firstName + " " + userr.secondName);
                            searchfornavname = false;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });



            register_item.setTitle("View Profile");
            login_item.setTitle("Logout");
            user.email = mAuth.getCurrentUser().getEmail();
            //tvNameNavbar = findViewById(R.id.tvNameNavBar);
            //tvEmailNavbar = findViewById(R.id.tvEmailNavBar);
            //tvEmailNavbar.setText(mAuth.getCurrentUser().getEmail());

            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("/Users"+mAuth.getCurrentUser().getUid());

            history.setVisible(true);
        }
        else{
            tvNameNavbar.setText("Not logged in.");
            tvEmailNavbar.setText("");
        }
        mNavView.getMenu().getItem(0).setChecked(false);

    }

    public void writeProfileStream(FileOutputStream out, ArrayList<String> readFrom)
    {
        OutputStreamWriter writer = new OutputStreamWriter(out);
        try {
            for (String s : readFrom) {
                for (char c : s.toCharArray()) {
                    writer.write((int) c);
                }
                writer.write((int)'\n');
            }
            writer.close();
            out.close();
        }
        catch (IOException e)
        {

        }
    }

    public ArrayList<String> readProfileStream(FileInputStream in)
    {
        ArrayList<String> arrL = new ArrayList<>();
        InputStreamReader inputStreamReader = new InputStreamReader(in);
        try {
            int c = inputStreamReader.read();
            while (c != -1) {
                String s = "";
                while((char)c != '\n')
                {
                    s += (char)c;
                    c = inputStreamReader.read();
                }
                arrL.add(s);
                c = inputStreamReader.read();
            }
            inputStreamReader.close();
            in.close();
        }
        catch (IOException e) {
        }
        return arrL;
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        mAuth = FirebaseAuth.getInstance();

        mNavView = findViewById(R.id.nav_view);
        mNavView.getMenu().getItem(0).setChecked(false);
        mNavView.getMenu().getItem(1).setChecked(false);
        mNavView.getMenu().getItem(2).setChecked(false);
        Menu menu = mNavView.getMenu();
        MenuItem register_item = menu.findItem(R.id.nav_register);
        MenuItem login_item = menu.findItem(R.id.nav_login);
        MenuItem history = menu.findItem(R.id.nav_history);
        if(mAuth.getCurrentUser() != null){
            register_item.setTitle("View Profile");
            login_item.setTitle("Logout");
            history.setVisible(true);

            tvEmailNavbar.setText(mAuth.getCurrentUser().getEmail());
            searchfornavname = true;
            FirebaseDatabase.getInstance().getReference().child("Users/"+ mAuth.getCurrentUser().getUid())
                    .addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(!searchfornavname)
                                return;
                            User userr = dataSnapshot.getValue(User.class);
                            tvNameNavbar.setText(userr.firstName + " " + userr.secondName);
                            searchfornavname = false;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


        }
        else {
            register_item.setTitle("Register");
            login_item.setTitle("Login");
            tvNameNavbar.setText("Not logged in.");
            tvEmailNavbar.setText("");
        }
    }
    //GifDrawable asad;
    public void popuplogin(final MenuItem item, final MenuItem registerItem, final MenuItem history){

        myDialogLogin= new Dialog(this);
        myDialogLogin.setContentView(R.layout.activity_log_in);
        myDialogLogin.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialogLogin.show();

        myDialogLogin.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                mNavView.getMenu().getItem(1).setChecked(false);
            }
        });
        progLogin = myDialogLogin.findViewById(R.id.progressBarLogin);
        progLogin.setVisibility(View.INVISIBLE);

        etEmail = myDialogLogin.findViewById(R.id.etEmail);
        etPassword = myDialogLogin.findViewById(R.id.etPassword);
        imgLoginBtn = myDialogLogin.findViewById(R.id.imgLoginBtn);

        mAuth = FirebaseAuth.getInstance();

        imgLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(item, registerItem, history);

            }
        });


        imgCloseLoginBtn = myDialogLogin.findViewById(R.id.ImgBtnCloseLoginBtn);
        imgCloseLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialogLogin.dismiss();
            }
        });
    }

    private void LogOut(final MenuItem item, MenuItem registerItem) {
        mNavView.getMenu().getItem(1).setChecked(false);
        if(mAuth.getCurrentUser() == null){
            Toast.makeText(MainActivity.this, "You are not logged in!", Toast.LENGTH_LONG).show();
        }
        else{
            FirebaseDatabase.getInstance().getReference("Users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child("token").setValue("");

            FirebaseAuth.getInstance().signOut();

            tvNameNavbar.setText("Not logged in.");
            tvEmailNavbar.setText("");

            Toast.makeText(MainActivity.this, "Sign out!", Toast.LENGTH_LONG).show();
            item.setTitle("Login");
            registerItem.setTitle("Register");

        }
    }

    public void login(final MenuItem item, final MenuItem registerItem, final MenuItem history) {

        password = etPassword.getText().toString();
        email = etEmail.getText().toString();

        if(email.isEmpty()) {
            etEmail.setError("Email cannot be empty");
            etEmail.requestFocus();
            return;
        }
        if(password.isEmpty())
        {
            etPassword.setError("Password cannot be empty");
            return;
        }
        imgLoginBtn.setClickable(false);
        if(mAuth.getCurrentUser() == null) {
            progLogin.setVisibility(View.VISIBLE);
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        okLogin = true;
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!okLogin)
                                    return;
                                okLogin = false;
                                User usr = dataSnapshot.getValue(User.class);
                                if(usr.token.isEmpty())
                                {
                                    imgLoginBtn.setClickable(true);
                                    Toast.makeText(MainActivity.this, "Logged In", Toast.LENGTH_LONG).show();
                                    myDialogLogin.dismiss();
                                    tvNameNavbar.setText(usr.firstName + " " + usr.secondName);
                                    tvEmailNavbar.setText(usr.email);
                                    item.setTitle("Logout");
                                    registerItem.setTitle("View Profile");
                                    history.setVisible(true);
                                    String token_id = FirebaseInstanceId.getInstance().getToken();
                                    FirebaseDatabase.getInstance().getReference("Users")
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                            .child("token").setValue(token_id);
                                    progLogin.setVisibility(View.INVISIBLE);
                                    okRefresh = true;
                                    FirebaseDatabase.getInstance().getReference().child("Notifications/" + mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (!okRefresh)
                                                return;
                                            okRefresh = false;
                                            Iterable<DataSnapshot>children = dataSnapshot.getChildren();
                                            for (DataSnapshot child:children) {
                                                LatitLongit loc = child.getValue(LatitLongit.class);
                                                if(!loc.isReceived)
                                                {
                                                    sendNotification(loc.latlang, loc.dogName, loc.date);
                                                    FirebaseDatabase.getInstance().getReference().child("Notifications/" + mAuth.getCurrentUser().getUid() + "/" + child.getKey()).child("isReceived").setValue(true);
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                                else
                                {
                                    imgLoginBtn.setClickable(true);
                                    Toast.makeText(MainActivity.this, "You are logged in on another device", Toast.LENGTH_LONG).show();
                                    FirebaseAuth.getInstance().signOut();
                                    progLogin.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
                        progLogin.setVisibility(View.INVISIBLE);
                        imgLoginBtn.setClickable(true);
                    }
                }
            });
        }
        else {
            Toast.makeText(MainActivity.this, "Already logged in!", Toast.LENGTH_LONG).show();
            imgLoginBtn.setClickable(true);
        }
    }

    public void sendNotification(String text, String whatDog, String date) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default");
        builder.setSmallIcon(R.mipmap.ic_launcher_logoo);
        builder.setContentTitle(whatDog + "'s location received");
        builder.setContentText(text);
        builder.setAutoCancel(true);
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + text + " (Found here)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            PendingIntent p = PendingIntent.getActivity(this, 0, mapIntent, 0);
            builder.setContentIntent(p);
        }
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(i2, builder.build());
        i2 = (i2+1)%100;

        try {
            VariableHolder.notifH = readProfileStream(openFileInput("notifHistory"));
            deleteFile("notifHistory");
        }catch (FileNotFoundException e) {
        }
        VariableHolder.notifH.add(whatDog + '|' + text + '~' + date);
        try {
            FileOutputStream notifOutputStream = openFileOutput("notifHistory", Context.MODE_PRIVATE);
            writeProfileStream(notifOutputStream, VariableHolder.notifH);
        }catch (FileNotFoundException e) {
        }
    }

    public String petNameToNotif;
    public void getUser(final String Tag) {

        ref = FirebaseDatabase.getInstance().getReference();
        okGetUserPet = true;
        final String[] ownerKey = new String[1];
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!okGetUserPet)
                    return;
                okGetUserPet = false;
                okGetUserUser = true;
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();

                for (DataSnapshot child:children) {
                    Pets pet = child.getValue(Pets.class);
                    if(pet.tag.equals(Tag))
                    {
                        ownerKey[0] = pet.ownerKey;
                        petNameToNotif = pet.petname;
                        ref1 = FirebaseDatabase.getInstance().getReference("Users/"+ownerKey[0]);
                        ref1.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(!okGetUserUser)
                                    return;
                                okGetUserUser = false;
                                user = dataSnapshot.getValue(User.class);
                                showUserProfile(user, dataSnapshot.getKey());
                                //findLocation(dataSnapshot.getKey(), petNameToNotif);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                        return;
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }


    public void btnDiscover(View view) {


        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();


            view.setBackgroundResource(R.drawable.paw_imag_800);

        }
        else {
            if(!checkBTPermissions())
                return;


            mBluetoothAdapter.startDiscovery();
            IntentFilter discoverDevicesIntent = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(mBroadcastReceiver3, discoverDevicesIntent);



            view.setBackground(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private boolean checkBTPermissions() {

        if (!mBluetoothAdapter.isEnabled()) {
            Intent startBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(startBluetooth, 0);
            //mBluetoothAdapter.enable();
            return false;
        }
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Please turn your location on")
                    .setCancelable(true)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, final int id) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
            return false;
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {

                this.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        }
        return true;
    }

    void makeTOAST(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    void startTimer() {

        timer = new Timer();

        timerTask = new TimerTask() {
            private Handler updateUI = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    if(!mBluetoothAdapter.isDiscovering())
                        (findViewById(R.id.button3)).setBackgroundResource(R.drawable.paw_imag_800);
                }
            };

            public void run() {
                try {
                    updateUI.sendEmptyMessage(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer.schedule(timerTask, 3000, 3000);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("default", "LocationNotification", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

    /*void sendNotification(String text) {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default");

        builder.setSmallIcon(R.drawable.ic_launcher_foreground);
        builder.setContentTitle("Location received");
        builder.setContentText(text);

        Uri gmmIntentUri = Uri.parse("geo:" + text);

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            PendingIntent p = PendingIntent.getActivity(this, 0, mapIntent, 0);
            builder.setContentIntent(p);
        }

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, builder.build());
    }*/

    @SuppressLint("MissingPermission")
    void findLocation(final String receiver_uid, final String petNameToNotif) {
        okLocation = true;
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null && okLocation) {
                            okLocation = false;
                            String latlang = location.getLatitude() + "," + location.getLongitude();
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Notifications").child(receiver_uid);

                            DateFormat df = new SimpleDateFormat("dd.MM HH:mm");
                            String date = df.format(Calendar.getInstance().getTime());
                            ref.push().setValue(new LatitLongit(latlang, petNameToNotif, date));
                        }
                    }
                });
        fusedLocationClient.getLastLocation().addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        makeTOAST("FAIL!");
                    }
                });
    }

    protected void AddToList(String text) {
        petDescriptions.add(text);
        devicesArrayList.add(text.substring(0, text.indexOf('|')));
        devicesArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
   //     getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
   //    MenuItem register = findViewById(R.id.nav_register);
       //register.setTitle("Buna");
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.

        Menu menu = mNavView.getMenu();
        MenuItem register_item = menu.findItem(R.id.nav_register);
        MenuItem history = menu.findItem(R.id.nav_history);
        int id = item.getItemId();


        if (id == R.id.nav_register) {
            if(item.getTitle().equals("Register")){
                Intent intent=new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
            if(item.getTitle().equals("View Profile")){
                Intent intent=new Intent(MainActivity.this, MyProfileActivity.class);
                startActivity(intent);
            }
        }
        else if (id == R.id.nav_login) {
            //popuplogin();
            if(item.getTitle().equals("Login")){
                popuplogin(item, register_item, history);

            }
            else{
                LogOut(item, register_item);

            }
        }
        else if(id == R.id.nav_history){
            item.setChecked(false);
            Intent intent = new Intent(MainActivity.this, ProfilesNotificationActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void showUserProfile(User user, final String uid){
        showProfile=new Dialog(this);
        showProfile.setContentView(R.layout.show_profile);
        showProfile.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        showProfile.show();

        imgCloseShowProfile= showProfile.findViewById(R.id.ImgBtnCloseShowProfileBtn);
        showname=showProfile.findViewById(R.id.name_profile);
        showemail=showProfile.findViewById(R.id.email_profile);
        showphone=showProfile.findViewById(R.id.phone_profile);
        showadress=showProfile.findViewById(R.id.adress_profile);
        Button sendNotif = showProfile.findViewById(R.id.sendNotification);

        String name="";
        String email="";
        String phone="";
        String adress="";

        if(user.isNamePublic)
            name=user.firstName + " " + user.secondName;
        else name="Name is not public";

        email=user.email;

        if(user.isPhonePublic)
            phone=user.phone;
        else phone="Phone number is not public";

        if(user.isAddressPublic)
            adress=user.address;
        else adress="Adress is not public";

        showname.setText(name);
        showemail.setText(email);
        showphone.setText(phone);
        showadress.setText(adress);

        sendNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findLocation(uid, petNameToNotif);
                Toast.makeText(MainActivity.this, "Notification sent", Toast.LENGTH_SHORT).show();
            }
        });

        imgCloseShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProfile.dismiss();
            }
        });


    }
}
