/**
 * Stabilirea bg-color la item in viewProfile, (daca e lost, culoarea nu se retine decat pe timpul activitatii)
 *
 *
 */


package com.example.fortech;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.graphics.Color.rgb;

public class MyProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{



    ArrayList<String> YourPetsName = new ArrayList<String>();
    ArrayList<Pets> YourPetsInfo = new ArrayList<Pets>();
    ArrayList<String> YourPetsKeys = new ArrayList<String>();
    Integer n;
    FirebaseAuth mAuth;
    DatabaseReference ref;
    String Uid;
    User user;
    TextView tvName, tvEmail, tvAddress, tvPhone, tvNameNavbar, tvEmailNavbar;
    ImageButton btnAddPet;
    NavigationView navigationView;
    ListView listView;
    ArrayAdapter arrayAdapter;
    Button imgCloseAddPetBtn, editProfile;
    public boolean okPopulatepetName = false, okRefreshProfile = false;


    void refreshProfileInfo()
    {
        okRefreshProfile = true;
        ref = FirebaseDatabase.getInstance().getReference("Users/"+Uid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!okRefreshProfile)
                    return;
                okRefreshProfile = false;
                user = dataSnapshot.getValue(User.class);
                fill_fields();
                tvNameHeader.setText(user.firstName + " " + user.secondName);
                tvEmailHeader.setText(user.email);
                modify_list = true;
                GetYourPets();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    View headerNavBar;

    TextView tvNameHeader, tvEmailHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myprofile_main);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        toolbar.setTitle("My Profile");

        mAuth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference();
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);
        tvAddress = findViewById(R.id.tvAddress);
        tvPhone = findViewById(R.id.tvPhone);
        btnAddPet = findViewById(R.id.imgBtnAddPet);

 //       tvNameNavbar = findViewById(R.id.tvNameNavBar);
//        tvEmailNavbar = findViewById(R.id.tvEmailNavBar);
 //       tvEmailNavbar.setText(mAuth.getCurrentUser().getEmail());

        tvName.setText("");
        tvEmail.setText("");
        tvAddress.setText("");
        tvPhone.setText("");



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
         navigationView = findViewById(R.id.nav_view);
       // setSupportActionBar(toolbar);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);


        Uid = mAuth.getCurrentUser().getUid();

        refreshProfileInfo();

        editProfile = findViewById(R.id.editProf);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyProfileActivity.this, EditCredentials.class);
                startActivity(intent);
            }
        });



        btnAddPet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfileActivity.this, AddPetActivity.class));
            }
        });

        Menu menu = navigationView.getMenu();
        MenuItem register_item = menu.findItem(R.id.nav_register);
        MenuItem login_item = menu.findItem(R.id.nav_login);
        MenuItem history = menu.findItem(R.id.nav_history);
        history.setVisible(true);
        register_item.setTitle("Home");
        login_item.setTitle("Logout");

        headerNavBar = navigationView.getHeaderView(0);
        tvNameHeader = headerNavBar.findViewById(R.id.tvNameNavBar);
        tvEmailHeader = headerNavBar.findViewById(R.id.tvEmailNavBar);



    }


    boolean modify_list = true;
    @Override
    protected void onRestart() {
        super.onRestart();


        navigationView.getMenu().getItem(0).setChecked(false);
        navigationView.getMenu().getItem(1).setChecked(false);
        navigationView.getMenu().getItem(2).setChecked(false);

        YourPetsName.clear();
        YourPetsInfo.clear();
        YourPetsKeys.clear();
        modify_list = true;
        refreshProfileInfo();
        GetYourPets();
    }

    void GetYourPets(){
        listView = findViewById(R.id.list_slideup);
        ref = FirebaseDatabase.getInstance().getReference("Pets");
        okPopulatepetName = true;
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!okPopulatepetName)
                    return;
                okPopulatepetName = false;
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();
                if(modify_list){
                    for (DataSnapshot child:children) {
                        Pets pet = child.getValue(Pets.class);
                        String pemail = pet.email;
                        if(pemail.equals(user.email)){
                            YourPetsName.add(pet.petname);
                            YourPetsInfo.add(pet);
                            YourPetsKeys.add(child.getKey());
                        }
                    }

                    SlideUp();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void SlideUp(){
        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, YourPetsName);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DeclareLostAlert(i);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                DeletePet(i);
                return true;
            }
        });


        listView.setOnScrollListener(new ListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if(YourPetsInfo.isEmpty())
                    return;
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if (lastInScreen == totalItemCount) {
                    for(int i = 0; i<totalItemCount; i++)
                    {
                        if(YourPetsInfo.get(i).isLost)
                        {
                            view.getChildAt(i).setBackgroundColor(rgb(249, 99, 83));
                        }
                        else
                        {
                            view.getChildAt(i).setBackgroundColor(rgb(255, 255, 255));
                        }
                    }

                } else {

                }
            }
        });
    }

    private void DeletePet(final int pozPet) {
        if(YourPetsInfo.isEmpty())
            return;
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AlertDIalog);
        String ms = "Delete " + YourPetsName.get(pozPet) + "?";
        alertDialog.setTitle("Not yours?")
                .setMessage(ms)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Pets/"+YourPetsKeys.get(pozPet));
                        ref.removeValue();

                        YourPetsKeys.remove(pozPet);
                        YourPetsName.remove(pozPet);
                        YourPetsInfo.remove(pozPet);

                        arrayAdapter.remove(pozPet);
                        arrayAdapter.notifyDataSetChanged();
                        if(YourPetsName.isEmpty())
                        {
                            arrayAdapter.add("No pets");
                            arrayAdapter.notifyDataSetChanged();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();

    }

    void DeclareLostAlert(final int pozLost){
        if(YourPetsInfo.isEmpty())
            return;
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this, R.style.AlertDIalog);
        if(!YourPetsInfo.get(pozLost).isLost) {

            String mes = "Have you lost " + YourPetsName.get(pozLost) + "?";
            alertDialog.setTitle("Lost dog")
                    .setMessage(mes)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Pets pet_declared = YourPetsInfo.get(pozLost);
                            pet_declared.isLost = true;
                            ref = FirebaseDatabase.getInstance().getReference().child("Pets/" + YourPetsKeys.get(pozLost));
                            modify_list = false;
                            ref.setValue(pet_declared);
                            int color = rgb(249, 99, 83);
                            listView.getChildAt(pozLost).setBackgroundColor(color);
                            arrayAdapter.notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            arrayAdapter.notifyDataSetChanged();
                            dialogInterface.cancel();
                        }
                    })
                    .show();
        }
        else{

            String mes = "Have you found " + YourPetsName.get(pozLost) + "?";
            alertDialog.setTitle("Found dog")
                    .setMessage(mes)
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Pets pet_declared = YourPetsInfo.get(pozLost);
                            pet_declared.isLost = false;
                            ref = FirebaseDatabase.getInstance().getReference().child("Pets/" + YourPetsKeys.get(pozLost));
                            modify_list = false;
                            ref.setValue(pet_declared);
                            int color = rgb(255, 255, 255);
                            listView.getChildAt(pozLost).setBackgroundColor(color);
                            arrayAdapter.notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.cancel();
                        }
                    })
                    .show();
        }
    }



    private void fill_fields() {
        tvName.setText(user.firstName + " " + user.secondName);
        tvEmail.setText(user.email);
        tvAddress.setText(user.address);
        tvPhone.setText(user.phone);
      //  tvNameNavbar.setText(user.firstName + " " + user.secondName);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //     getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
        //    MenuItem register = findViewById(R.id.nav_register);
        //register.setTitle("Buna");
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {
        // Handle navigation view item clicks here.

        Menu menu = navigationView.getMenu();
        MenuItem register_item = menu.findItem(R.id.nav_register);
        int id = item.getItemId();


        if (id == R.id.nav_register) {
            MyProfileActivity.super.finish();
        }
        else if (id == R.id.nav_login) {

            FirebaseDatabase.getInstance().getReference("Users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .child("token").setValue("");

            mAuth.signOut();
            tvNameHeader.setText("Not logged in.");
            tvEmailHeader.setText("");
            Toast.makeText(MyProfileActivity.this, "Sign out!", Toast.LENGTH_LONG).show();
            MyProfileActivity.super.finish();

        }
        else if(id == R.id.nav_history){
            Intent intent = new Intent(MyProfileActivity.this, ProfilesNotificationActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }




}
