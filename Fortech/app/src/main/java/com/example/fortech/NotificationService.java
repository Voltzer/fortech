package com.example.fortech;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class NotificationService extends FirebaseMessagingService {

    public NotificationService() {
    }

    public String path;
    private int i = 0;
    FileOutputStream notifOutputStream;

    public boolean ok = false;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        path = remoteMessage.getData().get("path_from");

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(path);

        ok = true;

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!ok)
                    return;
                ok = false;
                LatitLongit loc = dataSnapshot.getValue(LatitLongit.class);
                if(loc != null && !loc.isReceived) {
                    ref.child("isReceived").setValue(true);
                    sendNotification(loc.latlang, loc.dogName, loc.date);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void sendNotification(String text, String whatDog, String date) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default");
        builder.setSmallIcon(R.mipmap.ic_launcher_logoo);
        builder.setContentTitle(whatDog + "'s location received");
        builder.setContentText(text);
        builder.setAutoCancel(true);
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + text + " (Found here)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            PendingIntent p = PendingIntent.getActivity(this, 0, mapIntent, 0);
            builder.setContentIntent(p);
        }
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(i, builder.build());
        i = (i+1)%100;

        try {
            VariableHolder.notifH = readProfileStream(openFileInput("notifHistory"));
            deleteFile("notifHistory");
        }catch (FileNotFoundException e) {
        }
        VariableHolder.notifH.add(whatDog + '|' + text + '~' + date);
        try {
            notifOutputStream = openFileOutput("notifHistory", Context.MODE_PRIVATE);
            writeProfileStream(notifOutputStream, VariableHolder.notifH);
        }catch (FileNotFoundException e) {
        }
    }



    public void writeProfileStream(FileOutputStream out, ArrayList<String> readFrom)
    {
        OutputStreamWriter writer = new OutputStreamWriter(out);
        try {
            for (String s : readFrom) {
                for (char c : s.toCharArray()) {
                    writer.write((int) c);
                }
                writer.write((int)'\n');
            }
            writer.close();
            out.close();
        }
        catch (IOException e)
        {

        }
    }

    public ArrayList<String> readProfileStream(FileInputStream in)
    {
        ArrayList<String> arrL = new ArrayList<>();
        InputStreamReader inputStreamReader = new InputStreamReader(in);
        try {
            int c = inputStreamReader.read();
            while (c != -1) {
                String s = "";
                while((char)c != '\n')
                {
                    s += (char)c;
                    c = inputStreamReader.read();
                }
                arrL.add(s);
                c = inputStreamReader.read();
            }
            inputStreamReader.close();
            in.close();
        }
        catch (IOException e) {
        }
        return arrL;
    }


    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }
}
