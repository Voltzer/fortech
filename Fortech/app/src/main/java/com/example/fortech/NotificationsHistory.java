package com.example.fortech;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

class Notif{

    String title, location, date;
    Notif(String title, String location, String date){
        this.title = title;
        this.location = location;
        this.date = date;
    }
}
public class NotificationsHistory extends Fragment implements RVNotifAdapter.OnNoteListener {

    private List<Notif> Notifications;
    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_notifications, container, false);

        RecyclerView rv = view.findViewById(R.id.rvnotif);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);

        addToList();
        RVNotifAdapter adapter = new RVNotifAdapter(Notifications, this);
        rv.setAdapter(adapter);

        return view;
    }

    public void addToList(){
        Notifications = new ArrayList<>();

        for(String s : VariableHolder.notifH)
        {
            Notifications.add(new Notif(s.substring(0, s.indexOf('|')), s.substring(s.indexOf('|')+1, s.indexOf('~')), s.substring(s.indexOf('~')+1)));
        }

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Notifications/" + mAuth.getCurrentUser().getUid());
            ref.removeValue();
        }
    }

    @Override
    public void onNoteClick(int position) {
        sendNotification(Notifications.get(position).location, Notifications.get(position).title);
    }

    void sendNotification(String text, String whatDog) {
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + text + " (Found here)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
