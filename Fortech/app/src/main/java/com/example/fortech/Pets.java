package com.example.fortech;

public class Pets {

    public String ownerKey,email,petname,tag,description,city;
    public boolean isLost = false;

    public Pets()
    {

    }

    public Pets(String ownerKey,String email, String petname, String tag, String description, String city)
    {
        this.ownerKey = ownerKey;
        this.email = email;
        this.petname = petname;
        this.tag = tag;
        this.description = description;
        this.city = city;
    }


}
