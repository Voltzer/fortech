package com.example.fortech;

import android.app.Dialog;
import android.app.Person;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.AnimatorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

class Pet{

    String petName, petInfo;
    int photoId;
    String tag;

    Pet(String petName, String petInfo, int photoId, String tag){

        this.petName = petName;
        this.petInfo = petInfo;
        this.photoId = photoId;
        this.tag = tag;
    }
}


public class ProfilesHistory extends Fragment implements RVAdapter.OnNoteListener {

    private List<Pet> Animals;

    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profiles, container, false);


        RecyclerView rv = view.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);

        addToList();
        RVAdapter adapter = new RVAdapter(Animals, this);
        rv.setAdapter(adapter);
        return view;
    }


    private void addToList(){
        Animals = new ArrayList<>();
        for(String s : VariableHolder.profileH)
        {
            Animals.add(new Pet(s.substring(0, s.indexOf('|')), s.substring(s.indexOf('~')+1), R.drawable.profile_pic, s.substring(s.indexOf('|')+1, s.indexOf('~'))));
        }
    }

    public void getUser(final String Tag) {

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        final String[] ownerKey = new String[1];
        ref.child("Pets").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot>children = dataSnapshot.getChildren();

                for (DataSnapshot child:children) {
                    Pets pet = child.getValue(Pets.class);
                    if(pet.tag.equals(Tag))
                    {
                        ownerKey[0] = pet.ownerKey;
                        DatabaseReference ref1 = FirebaseDatabase.getInstance().getReference("Users/"+ownerKey[0]);
                        ref1.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                showUserProfile(user);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                        return;
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public Dialog showProfile;

    public void showUserProfile(User user){
        showProfile=new Dialog(getContext());
        showProfile.setContentView(R.layout.show_profile);
        showProfile.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        showProfile.show();

        ImageButton imgCloseShowProfile= showProfile.findViewById(R.id.ImgBtnCloseShowProfileBtn);
        TextView showname=showProfile.findViewById(R.id.name_profile);
        TextView showemail=showProfile.findViewById(R.id.email_profile);
        TextView showphone=showProfile.findViewById(R.id.phone_profile);
        TextView showadress=showProfile.findViewById(R.id.adress_profile);
        Button sendNotif = showProfile.findViewById(R.id.sendNotification);

        sendNotif.setVisibility(View.GONE);
        String name="";
        String email="";
        String phone="";
        String adress="";

        if(user.isNamePublic)
            name=user.firstName + " " + user.secondName;
        else name="Name is not public";

        email=user.email;

        if(user.isPhonePublic)
            phone=user.phone;
        else phone="Phone number is not public";

        if(user.isAddressPublic)
            adress=user.address;
        else adress="Adress is not public";

        showname.setText(name);
        showemail.setText(email);
        showphone.setText(phone);
        showadress.setText(adress);

        imgCloseShowProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProfile.dismiss();
            }
        });
    }

    @Override
    public void onNoteClick(int position) {
        getUser(Animals.get(position).tag);
    }
}
