package com.example.fortech;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.fortech.ui.main.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class ProfilesNotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiles_notification);

        Toolbar toolbar=findViewById(R.id.toolbarhistory);
        toolbar.setTitle("History");



        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfilesNotificationActivity.super.finish();
            }
        });

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        final ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        ImageButton b1 = new ImageButton(this);
        b1.setBackgroundResource(R.drawable.ic_delete_black_24dp);
        Toolbar.LayoutParams l3=new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
        l3.gravity=Gravity.END;
        l3.rightMargin = 40;
        b1.setLayoutParams(l3);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem() == 0)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfilesNotificationActivity.this, R.style.AlertDIalog);
                    alertDialog.setTitle("Are you sure you want to delete profiles history?")
                            .setMessage("Data deleted cannot be recovered.")
                            .setCancelable(true)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    VariableHolder.profileH.clear();
                                    deleteFile("profileHistory");
                                    ProfilesNotificationActivity.super.finish();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
                else
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ProfilesNotificationActivity.this, R.style.AlertDIalog);
                    alertDialog.setTitle("Are you sure you want to delete notification history?")
                            .setMessage("Data deleted cannot be recovered.")
                            .setCancelable(true)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    VariableHolder.notifH.clear();
                                    deleteFile("notifHistory");
                                    ProfilesNotificationActivity.super.finish();
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                }
            }
        });
        toolbar.addView(b1);

    }
}