package com.example.fortech;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.AnimalsViewHolder>{

    private OnNoteListener mOnNoteListener;

    public class AnimalsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView petname;
        TextView petinfo;
        ImageView petpicture;
        OnNoteListener onNoteListener;

        AnimalsViewHolder(View itemView, OnNoteListener onNoteListener){
            super(itemView);
            cv = itemView.findViewById(R.id.cv);
            petname = itemView.findViewById(R.id.pet_name);
            petinfo = itemView.findViewById(R.id.pet_info);
            petpicture = itemView.findViewById(R.id.pet_picture);
            this.onNoteListener = onNoteListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    List<Pet> Animals;

    RVAdapter(List<Pet> Animals, OnNoteListener onNoteListener) {

        this.Animals = Animals;
        this.mOnNoteListener = onNoteListener;
    }

    @Override
    public int getItemCount() {
        return Animals.size();
    }

    @Override
    public AnimalsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardviewactivity, viewGroup, false);
        AnimalsViewHolder avh = new AnimalsViewHolder(v, mOnNoteListener);
        return avh;
    }

    @Override
    public void onBindViewHolder(AnimalsViewHolder holder, int position) {
        holder.petname.setText(Animals.get(position).petName);
        holder.petinfo.setText(Animals.get(position).petInfo);
        holder.petpicture.setImageResource(Animals.get(position).photoId);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnNoteListener{
        void onNoteClick(int position);
    }
}