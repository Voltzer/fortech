package com.example.fortech;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RVNotifAdapter extends RecyclerView.Adapter<RVNotifAdapter.NotifHolder> {

    private OnNoteListener mOnNoteListener;

    public class NotifHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView title;
        TextView location;
        TextView date;
        OnNoteListener onNoteListener;

        NotifHolder(View itemView, OnNoteListener onNoteListener){
            super(itemView);
            cv = itemView.findViewById(R.id.cvnotif);
            title = itemView.findViewById(R.id.notiftitle);
            location = itemView.findViewById(R.id.notiflocation);
            date = itemView.findViewById(R.id.notifdate);
            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    List<Notif> Notifications;

    RVNotifAdapter(List<Notif> Notifications, OnNoteListener onNoteListener){
        this.Notifications=Notifications;
        this.mOnNoteListener=onNoteListener;
    }
    @NonNull
    @Override
    public NotifHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.nofif_cardview, viewGroup, false);
        NotifHolder nh = new NotifHolder(v, mOnNoteListener);
        return nh;
    }

    @Override
    public void onBindViewHolder(NotifHolder holder, int position) {
        holder.title.setText(Notifications.get(position).title);
        holder.location.setText(Notifications.get(position).location);
        holder.date.setText(Notifications.get(position).date);

    }

    @Override
    public int getItemCount() {
        return Notifications.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnNoteListener{
        void onNoteClick(int position);
    }
}
