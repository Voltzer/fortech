package com.example.fortech;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class RegisterActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private FirebaseDatabase mFirebaseDatabase;


    EditText etEmail, etFirstName, etSecondName, etPhone, etAddress, etPassword, etConfirmPW;
    String email, firstName, secondName, phone, address, password, Cpassword;
    boolean cphone, caddress, cname;
    Switch cbIsPhonePublic, cbIsAddressPublic, cbIsNamePublic;
    ProgressBar progReg;
    Button btnRegister, btnLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();

       // DatabaseReference myRef = mFirebaseDatabase.getReference("message");

       // myRef.setValue("Hello, World!");

        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPW = findViewById(R.id.etConfirmPW);
        etFirstName = findViewById(R.id.etFirstName);
        etSecondName = findViewById(R.id.etSecondName);
        etPhone = findViewById(R.id.etPhone);
        etAddress = findViewById(R.id.etAddress);
        btnRegister = findViewById(R.id.btnRegister);
        cbIsPhonePublic = findViewById(R.id.cbPhone);
        cbIsAddressPublic = findViewById(R.id.cbAddress);
        cbIsNamePublic = findViewById(R.id.cbName);
        progReg = findViewById(R.id.progressBarRegister);
        progReg.setVisibility(View.INVISIBLE);

        cbIsNamePublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsNamePublic.setText("Your name is public.");
                }
                else cbIsNamePublic.setText("Your name is not public.");
            }
        });


        cbIsAddressPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsAddressPublic.setText("Your address is public.");
                }
                else cbIsAddressPublic.setText("Your address is not public.");
            }
        });


        cbIsPhonePublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    cbIsPhonePublic.setText("Your phone is public.");
                }
                else cbIsPhonePublic.setText("Your phone is not public.");
            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });
    }

    void registerUser(){

        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        Cpassword = etConfirmPW.getText().toString();
        firstName = etFirstName.getText().toString();
        secondName = etSecondName.getText().toString();
        phone = etPhone.getText().toString();
        address = etAddress.getText().toString();
        cphone = cbIsPhonePublic.isChecked();
        caddress = cbIsAddressPublic.isChecked();
        cname = cbIsNamePublic.isChecked();

        if(email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError("Please enter a vaild email!");
            etEmail.requestFocus();
            return;
        }
        if(password.isEmpty()){
            etPassword.setError("Please enter a password!");
            etPassword.requestFocus();
            return;
        }
        else if(password.length() < 6)
        {
            etPassword.setError("Password must be at least 6 characters long");
            etPassword.requestFocus();
            return;
        }
        if(Cpassword.isEmpty()){
            etConfirmPW.setError("Please confirm your password!");
            etConfirmPW.requestFocus();
            return;
        }
        if(!password.equals(Cpassword)){
            etConfirmPW.setError("The password does not match!");
            etConfirmPW.requestFocus();
            return;
        }

        if(firstName.isEmpty()){
            etFirstName.setError("Please enter your first name!");
            etFirstName.requestFocus();
            return;
        }
        if(secondName.isEmpty()){
            etSecondName.setError("Please enter your second name!");
            etSecondName.requestFocus();
            return;
        }
        if(phone.isEmpty() && cphone){
            etPhone.setError("Pleasse enter your phone number!");
            etPhone.requestFocus();
            return;
        }
        if(address.isEmpty() && caddress){
            etAddress.setError("Please enter your address!");
            etAddress.requestFocus();
            return;
        }

        btnRegister.setClickable(false);
        progReg.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            String token_id = FirebaseInstanceId.getInstance().getToken();


                            User user = new User(email, firstName, secondName, phone, address, cphone, cname, caddress, token_id);


                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progReg.setVisibility(View.INVISIBLE);
                                    if(task.isSuccessful()){
                                        Toast.makeText(RegisterActivity.this, "Register complete!", Toast.LENGTH_LONG).show();
                                    }
                                    else Toast.makeText(RegisterActivity.this, "Error!", Toast.LENGTH_LONG).show();
                                    btnRegister.setClickable(true);
                                    RegisterActivity.super.finish();
                                }
                            });

                        }
                        else{
                            btnRegister.setClickable(true);
                            progReg.setVisibility(View.INVISIBLE);
                            Toast.makeText(RegisterActivity.this, "error!", Toast.LENGTH_LONG).show();
                        }
                    }

                });

    }

}
