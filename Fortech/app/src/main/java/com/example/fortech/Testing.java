package com.example.fortech;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pair;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Testing extends AppCompatActivity {

    EditText etKey, etSTR;
    TextView tvResult;
    Button encryptBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);
        encryptBtn = findViewById(R.id.encryptBtn);
        etKey = findViewById(R.id.etKey);
        etSTR = findViewById(R.id.etSTR);
        tvResult = findViewById(R.id.tvResult);
        encryptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cypher = etKey.getText().toString();
                if(cypher.isEmpty())
                {
                    Pair<String, String> pai = Encryption.Encryt(etSTR.getText().toString());
                    etKey.setText(pai.second);

                    tvResult.setText(pai.first);
                }
                else
                {
                    tvResult.setText(Encryption.Encryt(etSTR.getText().toString(), etKey.getText().toString()));
                }
            }
        });
    }
}
