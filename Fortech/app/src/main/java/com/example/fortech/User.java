package com.example.fortech;

public class User {
    public String token,email, firstName, secondName, phone, address;
    public boolean isPhonePublic, isNamePublic, isAddressPublic;

    public User(){

    }

    public User(String email, String firstName, String sName, String phone, String add, boolean isPhonePublic, boolean isNamePublic, boolean isAddressPublic, String tok){
        this.email = email;
        this.firstName = firstName;
        this.secondName = sName;
        this.phone = phone;
        this.address = add;
        this.isPhonePublic = isPhonePublic;
        this.isNamePublic = isNamePublic;
        this.isAddressPublic = isAddressPublic;
        this.token = tok;
    }
}
